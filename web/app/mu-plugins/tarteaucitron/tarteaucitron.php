<?php
/*
Plugin Name:  tarteaucitron.js – Cookies legislation & GDPR
Plugin URI:   https://opt-out.ferank.eu/fr/install/
Description:  tarteaucitron.js est le script le plus utilisé pour être en conformité avec les cookies et le RGPD.
Version:      1.0.0
Author:       Fidesio
Author URI:   https://www.fidesio.com/
License:      MIT License
*/

function tarteaucitron_scripts()
{
    // Register the script like this for a plugin:
    wp_register_script( 'tarteaucitron-script', plugins_url( '/tarteaucitron.js', __FILE__ ), array(), null, true );

    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script( 'tarteaucitron-script' );
}

function tarteaucitron_footer_scripts()
{
    ?>
    <script type="text/javascript">
        tarteaucitron.init({
            // "privacyUrl": "", /* Privacy policy url */

            "hashtag": "#tarteaucitron", /* Open the panel with this hashtag */
            "cookieName": "tarteaucitron", /* Cookie name */

            "orientation": "bottom", /* Banner position (top - bottom) */
            "showAlertSmall": false, /* Show the small banner on bottom right */
            "cookieslist": true, /* Show the cookie list */

            "adblocker": false, /* Show a Warning if an adblocker is detected */
            "AcceptAllCta" : true, /* Show the accept all button when highPrivacy on */
            "highPrivacy": false, /* Disable auto consent */
            "handleBrowserDNTRequest": false, /* If Do Not Track == 1, disallow all */

            "removeCredit": true, /* Remove credit link */
            // "moreInfoLink": true, /* Show more info link */
            "useExternalCss": false, /* If false, the tarteaucitron.css file will be loaded */

            // "cookieDomain": ".my-multisite-domaine.fr", /* Shared cookie for multisite */

            // "readmoreLink": "/cookiespolicy" /* Change the default readmore link */
        });

        // services
        (tarteaucitron.job = tarteaucitron.job || []).push('youtube');
        (tarteaucitron.job = tarteaucitron.job || []).push('recaptcha');
    </script>
    <?php
}

add_action( 'wp_enqueue_scripts', 'tarteaucitron_scripts', 99, 1 );
add_action( 'wp_footer', 'tarteaucitron_footer_scripts', 100, 1 );